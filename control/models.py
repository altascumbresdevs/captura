from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.conf import settings
import os.path
from django.core.files.storage import FileSystemStorage

fsusuarios = FileSystemStorage(location=os.path.dirname(__file__) + '/static/usuarios')
fsvehiculos = FileSystemStorage(location=os.path.dirname(__file__) + '/static/fsvehiculos')




class Perfil(models.Model):
    nombre = models.CharField(max_length=500)
    apellido_paterno = models.CharField(max_length=500, blank=True)
    apellido_materno = models.CharField(max_length=500, blank=True)
    tel1 = models.CharField(max_length=500, blank=True, null=True)
    correo = models.EmailField(max_length=500, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True)
    documento = models.ImageField(storage=fsusuarios, blank=True, null=True)

    def __str__(self):
        return self.nombre



class Captura(models.Model):
    nombre = models.CharField(max_length=500)
    apellido_paterno = models.CharField(max_length=500, blank=True)
    apellido_materno = models.CharField(max_length=500, blank=True)
    tel1 = models.CharField(max_length=500, blank=True, null=True)
    correo = models.EmailField(max_length=500, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.PROTECT, blank=True, null=True)
    documento = models.ImageField(storage=fsusuarios, blank=True, null=True)

    def __str__(self):
        return self.nombre
