from . import views
from . import datatables
from django.urls import include, re_path, path
from django.contrib.auth import views as auth_views

#from django.contrib.auth.decorators import permission_required
from .views import permission_required_2

urlpatterns = [
    path('recuperacion/',auth_views.PasswordResetView.as_view(template_name='registration/password_reset_form.html'),name='custom_pass_reset'),
    #path('recuperacion/hecho/(?P<uidb64>\d+)/(?P<token>\d+)$',auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_done.html'),name='password_reset_done'),
    path('recuperacion/', include('django.contrib.auth.urls')),
    #re_path(accounts/password_reset/ [name='password_reset']

    re_path(r'^$', views.index, name='index'),
    re_path(r'^login/$', views.login_page, name='login_page'),
    re_path(r'^login_user/$', views.login_user, name='login_user'),
    re_path(r'^logout/$', views.logout_user, name='logout_user'),
    re_path(r'^registro/$', views.registro, name='registro'),
    re_path(r'^registro/guardar$', views.registro_guardar, name='registro_guardar'),

    re_path(r'^capturar/insertar$', permission_required_2('captura.permisos-capturar-insertar', login_url='/captura/login') (views.capturar_insetar), name='capturar_insetar'),
    re_path(r'^capturar/insertar/save$', permission_required_2('captura.permisos-capturar-insertar', login_url='/captura/login') (views.capturar_insetar_save), name='capturar_insetar_save'),
    # re_path(r'^importacion/get_vin$', permission_required_2('demo.permisos-importacion-insertar', login_url='/demo/login') (views.get_vin), name='get_vin'),
    #Agregar marca
    # re_path(r'^importacion/agregar_marca$', (views.agregar_marca), name='agregar_marca'),

    #Reporte
    # re_path(r'^reporte_origen/(?P<id>\d+)$', (views.reporte_origen), name='reporte_origen'),


    re_path(r'^capturas/lista$', permission_required_2('captura.permisos-capturar-lista', login_url='/captura/login') (views.captura_lista), name='captura_lista'),


]