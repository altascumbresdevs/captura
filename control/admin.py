from django.contrib import admin
from .models import *
from django.db.models import Q
from django.contrib.auth.models import Permission
from django.contrib.auth.models import ContentType
# Register your models here.
admin.site.register(Perfil)
admin.site.register(Permission)
admin.site.register(ContentType)