from django_datatables_view.base_datatable_view import BaseDatatableView
from django.db.models import Q, Sum, Max
from .models import *
from django.db.models import OuterRef, Subquery
import datetime
from datetime import datetime,timedelta,date
from django.contrib.auth.models import Permission, Group, User
from django.db import connection


def replace_nones(auxarr):
    i = 0
    for a in auxarr:
        if a.replace(" ", "") == "None":
            auxarr[i] = ""
        i += 1
    return auxarr

class DTMisVehiculos(BaseDatatableView):
    columns = ['nombre']
    order_columns = ['nombre']

    def get_initial_queryset(self):

        obj = Vehiculos.objects.all()

        estatus = self.request.GET.get(u'estatus', '')
        if estatus != "0":
            obj = obj.filter(estatus=str(estatus))


        vin = self.request.GET.get(u'vin', '')
        if vin != "":
            obj = obj.filter(vin__icontains=str(vin))

        user = self.request.user
        user_group = self.request.user.groups.all().latest('id')
        grupo = Group.objects.get(id=int(user_group.id))
        impo = Group.objects.get(name="Importador")
        if grupo == impo:
            obj = obj.filter(usuario=user)


        return obj


    def filter_queryset(self, qs):
        search = self.request.GET.get(u'search[value]', None)
        if search:
            qs = qs.filter(Q(vin__icontains=search))
        return qs

    def prepare_results(self, qs):
        qs_descendente = Vehiculos.objects.filter(id__in=qs).order_by('-id')
        json_data = []
        for item in qs_descendente:
            if item.estatus == "1":
                estatus = "Pendiente por fotos"
                btn_descargar = ""
                btn_fotos = ""
            elif item.estatus == "2":
                estatus = "Pendiente por Verificar"
                btn_descargar = ""
                btn_fotos = ""
            elif item.estatus == "4":
                estatus = "Rechazado"
                btn_descargar = ""
                btn_fotos = ""
            else:
                estatus = "Listo para imprimir"
                btn_descargar = '<a href="../reporte_origen/'+str(item.id)+'" target="_blank"><i class="fas fa-file"></i></a>'
                fotos = [
                    str(item.foto1),
                    str(item.foto2),
                    str(item.foto3),
                ]
                btn_fotos = '<a href="javascript:void(0)" onclick="fotos(' + str(fotos) + ')"><i class="fas fa-camera-retro"></i></a>'
            
            if item.valor_captuador is not None:
                #valor_captuador = "$"+str(float(item.valor_captuador))
                valor_captuador = "$"+f'{float(item.valor_captuador):,}'
                #print(valor_captuador)
            else:
                valor_captuador = ""

            auxarr = [
                str(item.vin),
                str(item.marca),
                str(item.modelo),
                str(item.anio_ws),
                str(estatus),
                str(btn_fotos),
                str(valor_captuador),
                str(item.fecha_emision.strftime("%d-%m-%Y")),
                btn_descargar,
                '<a href="admin/'+ str(item.id) +'"><i class="fas fa-clipboard"></i></a>',
            ]
            auxarr = replace_nones(auxarr)
            json_data.append(auxarr)
        return json_data

