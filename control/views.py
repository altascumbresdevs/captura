from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Permission, ContentType, Group
from .models import *
from PIL import Image
from django.core.files.base import ContentFile
from functools import wraps
from urllib.parse import urlparse
from io import BytesIO
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.exceptions import PermissionDenied
from django.shortcuts import resolve_url
from django.contrib.auth.decorators import user_passes_test

from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib import messages
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect, JsonResponse
import json
from datetime import datetime,timedelta,date
from django.db.models import Q, Sum, Max, Min
import traceback
import requests
import base64
from django.core.files.base import ContentFile

from django.core.files.storage import FileSystemStorage



# Create your views here.
@login_required(login_url='/captura/login')
def index(request):
    user_group = request.user.groups.all().latest('id')
    grupo = Group.objects.get(id=int(user_group.id))

    return render(request,'index/index.html',{'grupo': grupo})

def login_page(request):
    return render(request,'login/nlogin.html')


def login_user(request):
    if request.method == 'POST':
        username = request.POST.get('user', '')
        password = request.POST.get('password', '')
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            #usrol = UserRole.objects.filter(user=user).latest('id')
            #request.session['rol'] = usrol.role.name
            role = ""
            if user.groups.all().exists():
                usrol = user.groups.all().latest('id')
                role = usrol.name
            login(request, user)
            request.session['rol'] = role
            request.session['menus'] = get_menu_permission(request)
            permissions = Permission.objects.filter(user=request.user)
            perfil = Perfil.objects.get(user__id=user.id)
            request.session['nombre'] = perfil.nombre
            request.session['fecha'] = str(datetime.today().strftime('%d-%m-%Y'))
            print(request.session)
            return redirect('/captura')
        else:
            return render(request,'login/nlogin.html',{'error': True, 'message': 'Nombre de usuario o contraseña incorrecta.'})

def registro(request):
    return render(request,'login/registro.html')
    


def registro_guardar(request):
    if request.method == 'POST':
        email = request.POST.get('email', '')
        password = request.POST.get('password', '')
        confirm_password = request.POST.get('confirm-password', '')
        nombre = request.POST.get('nombre', '')
        apellido = request.POST.get('apellido', '')
        telefono = request.POST.get('telefono', '')


        if password == confirm_password:
            if not User.objects.filter(username=email).exists():
                
                user = User.objects.create_user(username=email,email=email,password=password)

                objPerfil = Perfil.objects.create()
                objPerfil.nombre = nombre
                objPerfil.apellido_paterno = apellido
                objPerfil.tel1 = telefono
                objPerfil.correo = email
                objPerfil.user = user
                objPerfil.save()

                group = Group.objects.get(name='Importador')
                user.groups.add(group)
                user.save()
                
                #return redirect('/captura/login')
                return render(request,'login/nlogin.html',{'error': False,'registro': True, 'message': 'Usuario registrado. Ya puedes iniciar sesión.'})
            else:
                return render(request,'login/registro.html',{'error': True, 'message': 'Ya existe un registro con el correo electrónico ingresado. Recupera tu contraseña.'})   
        else:
            return render(request,'login/registro.html',{'error': True, 'message': 'La contraseña no coincide.'})
    else:
        return redirect('/captura')


@login_required(login_url='/captura/login')
def logout_user(request):
    logout(request)
    return redirect('/captura/login')



def get_menu_permission(request):
    menus = [
        {
            'nombre': "Principal",
            'has_perm': False,
            'icon': 'fa fa-home',
            'menu': [
                {
                    'nombre': 'Nueva captura',
                    'permiso': 'captura.permisos-capturar-insertar',
                    'url': 'capturar_insetar',
                    'multi_menu': False,
                    'has_perm': False,
                },
                {
                    'nombre': 'Mis Capturas',
                    'permiso': 'captura.permisos-capturar-lista',
                    'url': 'captura_lista',
                    'multi_menu': False,
                    'has_perm': False,
                },
                
            ]
        },
        
    ]
    try:
        user = request.user
        count1 = 0
        for menu in menus:
            has_perm_menu = False
            count2 = 0
            for submenu in menu['menu']:
                if submenu['multi_menu'] == False:
                    if user.has_perm(submenu['permiso']):
                        menus[count1]['menu'][count2]['has_perm'] = True
                        has_perm_menu = True
                else:
                    count3 = 0
                    has_perm_submenu = False
                    for subsubmenu in submenu['submenu']:
                        if user.has_perm(subsubmenu['permiso']):
                            menus[count1]['menu'][count2]['submenu'][count3]['has_perm'] = True
                            has_perm_submenu = True
                            has_perm_menu = True
                        count3 += 1
                    menus[count1]['menu'][count2]['has_perm'] = has_perm_submenu
                count2 += 1
            menus[count1]['has_perm'] = has_perm_menu
            count1 += 1
        print(menus)
        return menus
    except Exception as e:
        return menus
        


def permission_required_2(perm, login_url=None, raise_exception=False):
    """
    Decorator for views that checks whether a user has a particular permission
    enabled, redirecting to the log-in page if necessary.
    If the raise_exception parameter is given the PermissionDenied exception
    is raised.
    """
    def check_perms(user):
        if isinstance(perm, str):
            perms = (perm,)
        else:
            perms = perm
        # First check if the user has the permission (even anon users)
        if user.has_perms(perms):
            return True
        # In case the 403 handler should be called raise the exception
        if raise_exception:
            raise PermissionDenied
        # As the last resort, show the login form
        #return HttpResponseNotFound()
        return False
    return user_passes_test_2(check_perms, login_url=login_url)


def user_passes_test_2(test_func, login_url=None, redirect_field_name=REDIRECT_FIELD_NAME):
    """
    Decorator for views that checks that the user passes the given test,
    redirecting to the log-in page if necessary. The test should be a callable
    that takes the user object and returns True if the user passes.
    """

    def decorator(view_func):
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            if test_func(request.user):
                #if request.user.is_authenticated:
                #    return HttpResponseNotFound()
                return view_func(request, *args, **kwargs)
            if request.user.is_authenticated:
                return HttpResponseNotFound()
            else:
                path = request.build_absolute_uri()
                resolved_login_url = resolve_url(login_url or settings.LOGIN_URL)
                # If the login url is the same scheme and net location then just
                # use the path as the "next" url.
                login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
                current_scheme, current_netloc = urlparse(path)[:2]
                if ((not login_scheme or login_scheme == current_scheme) and
                        (not login_netloc or login_netloc == current_netloc)):
                    path = request.get_full_path()
                from django.contrib.auth.views import redirect_to_login
                return redirect_to_login(
                    path, resolved_login_url, redirect_field_name)
        return _wrapped_view
    return decorator



@login_required(login_url='/control/login')
def capturar_insetar(request):

    now = datetime.now()

    return render(request, 'captura/form.html',{})


@login_required(login_url='/control/login')
def capturar_insetar_save(request):
    try:
        vin = request.POST.get('vin', '')
        marca_us = request.POST.get('marca', '')
        anio_us = request.POST.get('anio', '')
        valor = request.POST.get('valor_limpio', '')
        fecha = request.POST.get('fecha', '')
        
        messages.error(request, 'Vehiculo agregado correctamente.', extra_tags='success')
        return redirect('vehiculos_lista')
    except Exception as e:
        messages.error(request, 'Error creando Vehiculo: ' + str(e), extra_tags='danger')
        return redirect('vehiculos_lista')


@login_required(login_url='/control/login')
def captura_lista(request):
    return render(request, 'vehiculos/list.html')
